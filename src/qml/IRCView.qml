/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.2
import QtGraphicalEffects 1.0

import org.kde.konversation.modelkonv 0.1 as ModelKonv

Item {
    property int dividerPos: 100

    onDividerPosChanged: {
        if (listView.atBottom) {
            scrollToBottomTimer.restart();
        }
    }

    property alias model: listView.model

    ShaderEffectSource {
        id: scrollViewWrapper

        anchors.fill: scrollView

        visible: false

        live: enabled
        hideSource: enabled

        sourceItem: scrollView
    }

    LinearGradient {
        id: maskGradient

        anchors.fill: scrollView

        visible: false

        start: Qt.point(0, 0)
        end: Qt.point(10, 0)

        gradient: Gradient {
            GradientStop { position: 0.0; color: "#00000000" }
            GradientStop { position: 1.0; color: "#FF000000" }
        }
    }

    OpacityMask {
        anchors.fill: scrollView

        source: scrollViewWrapper
        maskSource: maskGradient
    }

    ScrollView {
        id: scrollView

        anchors.fill: parent

        ListView {
            id: listView

            property bool atBottom: (visibleArea.yPosition == 1.0)

            focus: true

            snapMode: ListView.SnapToItem
            verticalLayoutDirection: ListView.BottomToTop

            onCountChanged: scrollToBottomTimer.restart();

            onWidthChanged: {
                if (atBottom) {
                    scrollToBottomTimer.restart();
                }
            }

            delegate: Item {
                width: listView.width
                height: message.height + 2

                clip: true

                ModelKonv.NickItem {
                    id: nick

                    width: 200
                    height: message.font.pixelSize + 10 // 10: Fake space for descenders.

                    anchors.right: message.left
                    anchors.rightMargin: dividerHandle.width

                    nick: model.nick
                }

                Label {
                    id: message

                    x: dividerPos + dividerHandle.width
                    width: listView.width - x

                    text: model.message

                    textFormat: Text.PlainText
                    wrapMode: Text.Wrap
                }
            }
        }
    }

    Timer {
        id: scrollToBottomTimer

        interval: 0
        repeat: false

        onTriggered: {
            listView.forceLayout();
            listView.positionViewAtBeginning();
        }
    }

    MouseArea {
        id: dividerHandle

        x: dividerPos

        width: 6
        height: parent.height

        property bool dragActive: drag.active // What a stupid API.

        onDragActiveChanged: {
            if (!dragActive) {
                dividerPos = x;
            }
        }

        hoverEnabled: listView.count

        cursorShape: Qt.SizeHorCursor

        drag.target: dividerHandle
        drag.axis: Drag.XAxis

        Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter

            width: 2
            height: parent.height

            color: "black"

            opacity: (dividerHandle.containsMouse || dividerHandle.drag.active) ? 1.0 : 0

            Behavior on opacity {
                NumberAnimation { duration: 300 }
            }
        }
    }
}
