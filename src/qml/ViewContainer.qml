/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.2

import org.kde.konversation.modelkonv 0.1 as ModelKonv

Item {
    width: tabBar.implicitWidth + 10
    height: 500

    ModelKonv.LogModel {
        id: log

        logUrl: gLogUrl

        onChannelListChanged: {
            if (tabs.current) {
                tabs.current.checked = false;
            }
        }
    }

    ModelKonv.ChannelFilterProxyModel {
        id: channelFilter

        logModel: log
    }

    Row {
        id: tabBar

        anchors {
            top: parent.top
            topMargin: 5
            horizontalCenter: parent.horizontalCenter
        }

        spacing: 5

        Button {
            id: refreshButton

            text: "Reload log"

            onClicked: {
                channelFilter.channel = "";
                log.refresh()
            }
        }

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter

            width: 1
            height: refreshButton.height - 2

            color: "darkgrey"
        }

        ExclusiveGroup { id: tabs }

        Repeater {
            model: log.channelList

            delegate: Button {
                text: modelData
                checkable: true
                exclusiveGroup: tabs

                onClicked: channelFilter.channel = text;
            }
        }
    }

    Label {
        anchors.fill: view

        visible: !channelFilter.channel

        text: "Click a channel button."
        textFormat: Text.PlainText
        font.pointSize: 16

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    IRCView {
        id: view

        model: channelFilter

        anchors {
            top: tabBar.bottom
            topMargin: 10
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
    }
}
