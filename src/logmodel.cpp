/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "logmodel.h"

#include <QCollator>
#include <QFile>
#include <QSet>

LogModel::LogModel(QObject *parent) : QAbstractListModel(parent)
{
}

LogModel::~LogModel()
{
}

QHash< int, QByteArray > LogModel::roleNames() const
{
    return staticRoleNames();
}

QHash< int, QByteArray > LogModel::staticRoleNames()
{
    QHash<int, QByteArray> roleNames;
    roleNames[NickRole] = "nick";
    roleNames[MessageRole] = "message";

    return roleNames;
}

QUrl LogModel::logUrl() const
{
    return m_logUrl;
}

void LogModel::setLogUrl(const QUrl& url)
{
    if (m_logUrl != url) {
        m_logUrl = url;

        refresh();

        emit logUrlChanged();
    }
}

QStringList LogModel::channelList() const
{
    return m_channelList;
}

Channel *LogModel::channel(const QString& name) const
{
    if (m_channels.contains(name)) {
        return m_channels[name];
    }

    return 0;
}

QVariant LogModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const int row = index.row();

    if (m_logLines.count() > row) {
        const LogLine *line = m_logLines.at(row);

        if (role == NickRole) {
            return QVariant::fromValue(line->nick);
        } else if (role == MessageRole) {
            return line->message;
        }
    }

    return QVariant();
}

int LogModel::rowCount(const QModelIndex& parent) const
{
    if (!parent.isValid()) {
        return m_logLines.count();
    }

    return 0;
}

QModelIndex LogModel::index(int row, int column, const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    return createIndex(row, column, m_logLines.at(row));
}

void LogModel::refresh()
{
    emit beginResetModel();

    qDeleteAll(m_nicks);
    m_nicks.clear();

    qDeleteAll(m_channels);
    m_channels.clear();

    qDeleteAll(m_logLines);
    m_logLines.clear();
    m_channelList.clear();

    QFile logFile(m_logUrl.toLocalFile());

    if (!logFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QSet<QString> channels;

    while (!logFile.atEnd()) {
        const QByteArray lineData = logFile.readLine();
        QString lineText = QString::fromUtf8(lineData).trimmed();

        int sepIdx = lineText.indexOf(QChar(','));
        const QString channel = lineText.left(sepIdx);
        lineText = lineText.mid(sepIdx + 1);
        sepIdx = lineText.indexOf(QChar('>'));
        const QString nick = lineText.left(sepIdx + 1);
        const QString message = lineText.mid(sepIdx + 2);

        Nick *nickPtr = 0;

        if (!m_nicks.contains(nick)) {
            nickPtr = new Nick;
            nickPtr->text = nick;
            m_nicks[nickPtr->text] = nickPtr;
        } else {
            nickPtr = m_nicks[nick];
        }

        Channel *channelPtr = 0;

        if (!m_channels.contains(channel)) {
            channelPtr = new Channel;
            channelPtr->text = channel;
            m_channels[channelPtr->text] = channelPtr;
        } else {
            channelPtr = m_channels[channel];
        }

        LogLine *logLine = new LogLine;
        logLine->nick = nickPtr;
        logLine->channel = channelPtr;
        logLine->message = message;

        m_logLines.prepend(logLine);
        channels << channel;
    }

    emit endResetModel();

    m_channelList = QList<QString>::fromSet(channels);

    QCollator c;

    std::sort(m_channelList.begin(), m_channelList.end(),
        [&c](QString a, QString b) { return c.compare(a, b) < 0; });

    emit channelListChanged();
}
