/***************************************************************************
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@blue-systems.com>         *
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef NICKITEM_H
#define NICKITEM_H

#include <QSGTexture>
#include <QSharedPointer>
#include <QQuickItem>
#include <QQuickWindow>
#include <QSGSimpleTextureNode>
#include <QWeakPointer>

#include "logmodel.h"

// Derived from KDeclarative's ImageTexturesCache.
class SharedTextureCache
{
    public:
        SharedTextureCache();
        ~SharedTextureCache();

        QSharedPointer<QSGTexture> fetch(QQuickWindow *window, const Nick *nick, const QRect &rect);

    private:
        QHash<const Nick *, QHash<QWindow*, QWeakPointer<QSGTexture> > > m_cache;
};

// Derived from KDeclarative's ManagedTextureNode.
class ManagedTextureNode : public QSGSimpleTextureNode
{
    Q_DISABLE_COPY(ManagedTextureNode)

    public:
        ManagedTextureNode();
        ~ManagedTextureNode();

        void setTexture(QSharedPointer<QSGTexture> texture);

    private:
        QSharedPointer<QSGTexture> m_texture;
};

class NickItem : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QVariant nick READ nick WRITE setNick NOTIFY nickChanged)

    public:
        NickItem(QQuickItem *parent = 0);
        ~NickItem();

        QVariant nick() const;
        void setNick(const QVariant &nick);

    Q_SIGNALS:
        void nickChanged();

    protected:
        QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData);

    private:
        Nick *m_nick;
        bool m_nickChanged;
};

#endif
