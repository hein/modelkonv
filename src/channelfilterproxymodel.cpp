/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "channelfilterproxymodel.h"
#include "logmodel.h"

ChannelFilterProxyModel::ChannelFilterProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    m_channel = 0;
}

ChannelFilterProxyModel::~ChannelFilterProxyModel()
{
}

QString ChannelFilterProxyModel::channel() const
{
    return m_channel ? m_channel->text : QString();
}

void ChannelFilterProxyModel::setChannel(const QString& channel)
{
    if (!m_channel || m_channel->text != channel) {
        m_channel = channel.isEmpty() ? 0 : static_cast<LogModel *>(sourceModel())->channel(channel);

        invalidateFilter();

        emit channelChanged();
    }
}

LogModel* ChannelFilterProxyModel::logModel() const
{
    return static_cast<LogModel *>(sourceModel());
}

void ChannelFilterProxyModel::setLogModel(LogModel* model)
{
    if (sourceModel() != model) {
        setSourceModel(model);

        emit logModelChanged();
    }
}

bool ChannelFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (!sourceModel() || !m_channel || m_channel->text.isEmpty()) {
        return false;
    }

    const QModelIndex &idx = sourceModel()->index(sourceRow, 0, sourceParent);

    if (idx.isValid() && static_cast<LogLine *>(idx.internalPointer())->channel == m_channel) {
        return true;
    }

    return false;
}
