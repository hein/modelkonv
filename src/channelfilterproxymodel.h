/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef CHANNELFILTERPROXYMODEL_H
#define CHANNELFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

#include "logmodel.h"

class ChannelFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

    Q_PROPERTY(QString channel READ channel WRITE setChannel NOTIFY channelChanged)
    Q_PROPERTY(LogModel* logModel READ logModel WRITE setLogModel NOTIFY logModelChanged)

    public:
        ChannelFilterProxyModel(QObject *parent = 0);
        ~ChannelFilterProxyModel();

        QString channel() const;
        void setChannel(const QString &channel);

        LogModel *logModel() const;
        void setLogModel(LogModel *model);

    Q_SIGNALS:
        void channelChanged() const;
        void logModelChanged() const;

    protected:
        bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

    private:
        Channel *m_channel;
};

#endif
