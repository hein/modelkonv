/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef LOGMODEL_H
#define LOGMODEL_H

#include <QAbstractListModel>
#include <QUrl>

struct Nick
{
    QString text;
};

Q_DECLARE_METATYPE(Nick*)

struct Channel
{
    QString text;
};

struct LogLine
{
    Nick* nick;
    Channel* channel;
    QString message;
};

class LogModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QUrl logUrl READ logUrl WRITE setLogUrl NOTIFY logUrlChanged)
    Q_PROPERTY(QStringList channelList READ channelList NOTIFY channelListChanged)

    public:
        enum DataRole {
            NickRole = Qt::UserRole + 1,
            MessageRole
        };

        LogModel(QObject *parent = 0);
        ~LogModel();

        QHash<int, QByteArray> roleNames() const;
        static QHash<int, QByteArray> staticRoleNames();

        QUrl logUrl() const;
        void setLogUrl(const QUrl &url);

        QStringList channelList() const;
        Channel *channel(const QString &name) const;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        int rowCount(const QModelIndex &parent = QModelIndex()) const;

        QModelIndex index(int row, int column = 0, const QModelIndex &parent = QModelIndex()) const;

        Q_INVOKABLE void refresh();

    Q_SIGNALS:
        void refreshed() const;
        void logUrlChanged() const;
        void channelListChanged() const;

    private:
        QUrl m_logUrl;
        QHash<const QString, Nick *> m_nicks;
        QHash<const QString, Channel *> m_channels;
        QList<LogLine *> m_logLines;
        QStringList m_channelList;
};

#endif
