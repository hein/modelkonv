/***************************************************************************
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@blue-systems.com>         *
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "nickitem.h"

#include <QImage>
#include <QPainter>

Q_GLOBAL_STATIC(SharedTextureCache, sharedTextureCache)

SharedTextureCache::SharedTextureCache()
{
}

SharedTextureCache::~SharedTextureCache()
{
}

QSharedPointer<QSGTexture> SharedTextureCache::fetch(QQuickWindow *window, const Nick* nick, const QRect &rect)
{
    QSharedPointer<QSGTexture> texture = m_cache.value(nick).value(window).toStrongRef();

    if (!texture) {
        auto cleanAndDelete = [this, window, nick](QSGTexture* texture) {
            QHash<QWindow*, QWeakPointer<QSGTexture> >& textures = m_cache[nick];

            textures.remove(window);

            if (textures.isEmpty())
                m_cache.remove(nick);

            delete texture;
        };

        const QSize &size = rect.size();

        QImage image(size, QImage::Format_RGBA8888_Premultiplied);
        image.fill(Qt::transparent);

        QPainter painter(&image);

        painter.setRenderHint(QPainter::TextAntialiasing, true);
        painter.setBackground(QBrush(Qt::white));
        painter.setBackgroundMode(Qt::OpaqueMode);
        painter.drawText(rect, Qt::AlignRight, nick->text);

        texture = QSharedPointer<QSGTexture>(window->createTextureFromImage(image), cleanAndDelete);

        (m_cache)[nick][window] = texture.toWeakRef();
    }

    return texture;
}

ManagedTextureNode::ManagedTextureNode()
{
}

ManagedTextureNode::~ManagedTextureNode()
{
}

void ManagedTextureNode::setTexture(QSharedPointer<QSGTexture> texture)
{
    m_texture = texture;
    QSGSimpleTextureNode::setTexture(texture.data());
}

NickItem::NickItem(QQuickItem *parent) : QQuickItem(parent)
{
    m_nick = 0;
    m_nickChanged = false;

    setFlag(ItemHasContents, true);
}

NickItem::~NickItem()
{
}

QVariant NickItem::nick() const
{
    return QVariant::fromValue(m_nick);
}

void NickItem::setNick(const QVariant &nick)
{
    Nick* newNick = nick.value<Nick*>();

    if (m_nick != newNick) {
        m_nick = newNick;

        m_nickChanged = true;

        update();

        emit nickChanged();
    }
}

QSGNode *NickItem::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData)

    if (!window()) {
        delete oldNode;
        return 0;
    }

    ManagedTextureNode *node = static_cast<ManagedTextureNode *>(oldNode);

    if (!node) {
        delete oldNode;

        node = new ManagedTextureNode;
        node->setFiltering(QSGTexture::Nearest);
        node->setRect(boundingRect());
    }

    if (m_nickChanged) {
        node->setTexture(sharedTextureCache->fetch(window(), m_nick, boundingRect().toRect()));

        m_nickChanged = false;
    }

    return node;
}
