/***************************************************************************
 *   Copyright (C) 2015 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include <QDebug>
#include <QApplication>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>
#include <QStandardPaths>

#include "channelfilterproxymodel.h"
#include "logmodel.h"
#include "nickitem.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName(QLatin1String("modelkonv"));
    app.setApplicationVersion(QLatin1String("0.1"));

    qRegisterMetaType<Nick>("Nick");

    QLatin1String importUri("org.kde.konversation.modelkonv");
    qmlRegisterType<ChannelFilterProxyModel>(importUri.latin1(), 0, 1, "ChannelFilterProxyModel");
    qmlRegisterType<LogModel>(importUri.latin1(), 0, 1, "LogModel");
    qmlRegisterType<NickItem>(importUri.latin1(), 0, 1, "NickItem");

    QQuickView mainWindow;
    mainWindow.setResizeMode(QQuickView::SizeRootObjectToView);

    const QString &logPath = QStandardPaths::locate(QStandardPaths::AppDataLocation, QLatin1String("log.txt"));
    qDebug() << "Log path:" << logPath;
    mainWindow.rootContext()->setContextProperty(QLatin1String("gLogUrl"), QUrl::fromLocalFile(logPath));

    const QString &scenePath = QStandardPaths::locate(QStandardPaths::AppDataLocation, QLatin1String("ViewContainer.qml"));
    qDebug() << "Scene path:" << scenePath;
    mainWindow.setSource(QUrl::fromLocalFile(scenePath));

    mainWindow.show();

    return app.exec();
}
